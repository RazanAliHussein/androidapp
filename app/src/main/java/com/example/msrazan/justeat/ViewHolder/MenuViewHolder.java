package com.example.msrazan.justeat.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.msrazan.justeat.Interface.ItemClickListener;
import com.example.msrazan.justeat.R;

/**
 * Created by MS RAZAN on 3/10/2018.
 */

public class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {


    public TextView txtMenuName;
    public ImageView imageView;
    private ItemClickListener itemClickListener;

    public MenuViewHolder(View itemView){
        super(itemView);
        txtMenuName =(TextView)itemView.findViewById(R.id.menu_name);
         imageView =(ImageView)itemView.findViewById(R.id.menu_image);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void onClick(View view){
        itemClickListener.onClick(view,getAdapterPosition(),false);

    }
}
