package com.example.msrazan.justeat;

import android.content.ClipData;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.msrazan.justeat.Common.Common;
import com.example.msrazan.justeat.Database.Database;
import com.example.msrazan.justeat.Helper.RecyclerItemTouchHelper;
import com.example.msrazan.justeat.Interface.RecyclerItemTouchHelperListener;
import com.example.msrazan.justeat.Model.Order;
import com.example.msrazan.justeat.Model.Request;
import com.example.msrazan.justeat.ViewHolder.CartAdapter;
import com.example.msrazan.justeat.ViewHolder.CartViewHolder;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Cart extends AppCompatActivity implements RecyclerItemTouchHelperListener {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseDatabase database;
    DatabaseReference requests;

    public TextView txtTotalPrice;
    Button btnPlace;
    RelativeLayout rootLayout;




    List<Order> cart=new ArrayList<>();
    CartAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        database=FirebaseDatabase.getInstance();
        requests=database.getReference("Requests");

        recyclerView=(RecyclerView)findViewById(R.id.listCart);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        rootLayout=(RelativeLayout)findViewById(R.id.rootLayout);

        ItemTouchHelper.SimpleCallback itemTouchHelpercallback=new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this);
        new ItemTouchHelper(itemTouchHelpercallback).attachToRecyclerView(recyclerView);

        txtTotalPrice=(TextView)findViewById(R.id.total);
        btnPlace=(Button)findViewById(R.id.btnPlaceOrde);

        btnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               showAlertDialog();

            }
        });
        loadListFood();

    }

    private void showAlertDialog(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Cart.this);
        alertDialog.setTitle("One more step");
        alertDialog.setMessage("Enter your table number or address");
        final EditText edtaddress = new EditText(Cart.this);
        LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT

        );
        edtaddress.setLayoutParams(lp);
        alertDialog.setView(edtaddress);
        alertDialog.setIcon(R.drawable.ic_shopping_cart_black_24dp);
        alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Request request=new Request(
                        Common.CurrentUser.getPhone(),
                        Common.CurrentUser.getName(),
                        edtaddress.getText().toString(),
                        txtTotalPrice.getText().toString(),
                        cart
                );
                requests.child(String.valueOf(System.currentTimeMillis()))
                        .setValue(request);

                new Database(getBaseContext()).cleanCart();

                Toast.makeText(Cart.this,"Your order submitted Thank you ",Toast.LENGTH_SHORT).show();
                finish();

            }
        });
        alertDialog.setNegativeButton("No ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();


    }

    private void loadListFood(){

        cart = new Database(this).getCarts();
        adapter =new CartAdapter(cart,this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        int total=0;
        for(Order order:cart)
            total+=(Integer.parseInt(order.getPrice()))*(Integer.parseInt(order.getQuantity()));
        Locale locale = new Locale("en","US");
        NumberFormat fmt=NumberFormat.getCurrencyInstance(locale);
        txtTotalPrice.setText(fmt.format(total));

    }
    private void deleteCart(int position){
        cart.remove(position);
        new Database(this).cleanCart();
        for(Order item:cart)
            new Database(this).addToCart(item);
        loadListFood();

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
       if(viewHolder instanceof CartViewHolder){
           String name=((CartAdapter)recyclerView.getAdapter()).getItem(viewHolder.getAdapterPosition()).getProductName();
           final Order deleteitem=((CartAdapter)recyclerView.getAdapter()).getItem(viewHolder.getAdapterPosition());
           final int deleteIndex=viewHolder.getAdapterPosition();
           adapter.removeItem(deleteIndex);
           new Database(getBaseContext()).removeFromCart(deleteitem.getProductId());

           int total=0;
           List<Order>orders=new Database(getBaseContext()).getCarts();
           for(Order item:orders)
               total+=(Integer.parseInt(item.getPrice()))*(Integer.parseInt(item.getQuantity()));
           Locale locale = new Locale("en","US");
           NumberFormat fmt=NumberFormat.getCurrencyInstance(locale);
           txtTotalPrice.setText(fmt.format(total));

           Snackbar snackbar=Snackbar.make(rootLayout,name+"removed from cart!",Snackbar.LENGTH_LONG);

        snackbar.setAction("UNDO", new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        adapter.restoreItem(deleteitem,deleteIndex);
        new Database(getBaseContext()).addToCart(deleteitem);

        int total=0;
        List<Order>orders=new Database(getBaseContext()).getCarts();
        for(Order item:orders)
            total+=(Integer.parseInt(item.getPrice()))*(Integer.parseInt(item.getQuantity()));
        Locale locale = new Locale("en","US");
        NumberFormat fmt=NumberFormat.getCurrencyInstance(locale);
        txtTotalPrice.setText(fmt.format(total));


    }
});
        snackbar.setActionTextColor(Color.YELLOW);
        snackbar.show();
       }
    }
}


