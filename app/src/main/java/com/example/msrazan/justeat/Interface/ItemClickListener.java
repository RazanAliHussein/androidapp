package com.example.msrazan.justeat.Interface;

import android.view.View;

/**
 * Created by MS RAZAN on 3/10/2018.
 */

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
