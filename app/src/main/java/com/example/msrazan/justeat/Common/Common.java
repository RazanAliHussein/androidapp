package com.example.msrazan.justeat.Common;

import com.example.msrazan.justeat.Model.User;

/**
 * Created by MS RAZAN on 3/10/2018.
 */

public class Common {
    public  static User CurrentUser;

    public static String convertCodetostatus(String status) {
        if(status.equals("0"))
            return  "Placed";
        else if(status.equals("1"))
            return  "On my way";
        else
            return "Shipped";

    }
}
