package com.example.msrazan.justeat.ViewHolder;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.msrazan.justeat.Cart;
import com.example.msrazan.justeat.Common.Common;
import com.example.msrazan.justeat.Database.Database;
import com.example.msrazan.justeat.Interface.ItemClickListener;
import com.example.msrazan.justeat.Model.Order;
import com.example.msrazan.justeat.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by MS RAZAN on 3/14/2018.
 */

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> {

    private List<Order> ListData = new ArrayList<>();
    private Cart cart;

    public CartAdapter(List<Order> listData, Cart cart) {
        this.ListData = listData;
        this.cart = cart;
    }

    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater inflater=LayoutInflater.from(cart);
        View itemView = inflater.inflate(R.layout.cart_layout,parent,false);
        return new CartViewHolder(itemView);
    }
    public void onBindViewHolder(CartViewHolder holder, final int position){
       // TextDrawable drawable =TextDrawable.builder()
        //        .buildRound(""+ListData.get(position).getQuantity(), Color.RED);
      //  holder.img_cart_count.setImageDrawable(drawable);


      /*  holder.btn_quantity.setNumber(ListData.get(position).getQuantity());
        holder.btn_quantity.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                Order order=ListData.get(position);
                order.setQuantity(String.valueOf(newValue));
                new Database(cart).updateCart(order);


                List<Order> orders=new Database(cart).getCarts();
                int total=0;
                for(Order item:orders)
                    total+=(Integer.parseInt(order.getPrice()))*(Integer.parseInt(item.getQuantity()));
                Locale locale = new Locale("en","US");
                NumberFormat fmt=NumberFormat.getCurrencyInstance(locale);
                cart.txtTotalPrice.setText(fmt.format(total));

            }
        });*/

        Locale locale = new Locale("en","US");
        NumberFormat fmt=NumberFormat.getCurrencyInstance(locale);
        int price =(Integer.parseInt(ListData.get(position).getPrice()))
                *(Integer.parseInt(ListData.get(position).getQuantity()));
        holder.txt_price .setText(fmt.format(price));
        holder.txt_cart_name.setText(ListData.get(position).getProductName());




    }
    public int getItemCount(){
        return ListData.size();

    }

    public void removeItem(int position){
      ListData.remove(position);
      notifyItemRemoved(position);
    }
    public void restoreItem(Order item,int position){
        ListData.add(position,item);
        notifyItemInserted(position);
    }
    public Order getItem(int position){return ListData.get(position);}
}
