package com.example.msrazan.justeat.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.msrazan.justeat.Common.Common;
import com.example.msrazan.justeat.Interface.ItemClickListener;
import com.example.msrazan.justeat.R;

/**
 * Created by MS RAZAN on 5/8/2018.
 */

public class CartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public TextView txt_cart_name,txt_price;
    public ElegantNumberButton btn_quantity;
    private ItemClickListener itemClickListener;

    public RelativeLayout view_background;
    public LinearLayout view_forground;


    public void setTxt_cart_name(TextView txt_cart_name){

        this.txt_cart_name=txt_cart_name;
    }


    public CartViewHolder(View itemView){
        super(itemView);

        txt_cart_name=(TextView)itemView.findViewById(R.id.cart_item_name);
        txt_price=(TextView)itemView.findViewById(R.id.cart_item_Price);
       /* btn_quantity=(ElegantNumberButton) itemView.findViewById(R.id.btn_quantity);*/
        view_background=(RelativeLayout)itemView.findViewById(R.id.view_background);
        view_forground=(LinearLayout)itemView.findViewById(R.id.view_foreground);
    }



    public void onClick(View view){

    }

    public void onCreateContextMenu(ContextMenu contextMenu,View view,ContextMenu.ContextMenuInfo contextMenuInfo){
        contextMenu.setHeaderTitle("Select Action");
       // contextMenu.add(0,0,getAdapterPosition(), Common.Delete);
    }

}

