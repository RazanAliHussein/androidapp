package com.example.msrazan.justeat.Interface;

import android.support.v7.widget.RecyclerView;

/**
 * Created by MS RAZAN on 5/8/2018.
 */

public interface RecyclerItemTouchHelperListener {
    void onSwiped(RecyclerView.ViewHolder viewHolder,int direction,int position);
}
